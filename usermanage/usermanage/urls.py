from rest_framework.routers import SimpleRouter
from users.views import UserViewSet, AddressesViewSet

router = SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'addressess', AddressesViewSet)

urlpatterns = router.urls