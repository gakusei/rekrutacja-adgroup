# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class Users(models.Model):
    name = models.CharField(max_length=10, unique=True)
    adresses = models.ManyToManyField('Addresses', blank=True)


class Addresses(models.Model):
    city = models.CharField(max_length=255)
    street = models.CharField(max_length=255)
