# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from .models import Users, Addresses


class UserManageTests(APITestCase):
    def setUp(self):
        self.producer = Users.objects.create(name="Test")
        self.beer = Addresses.objects.create(city="TestCity", street="TestStreet")

    def test_get_user_list(self):
        url = reverse('users-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_user(self):
        response = self.client.get('/users/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_user(self):
        url = reverse('users-list')
        data = {'name': 'Test2'}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Users.objects.count(), 2)
        self.assertEqual(Users.objects.get(id=2).name, 'Test2')

    def test_create_address(self):
        url = reverse('addresses-list')
        data = {'city': 'TestCity2', "street":"TestStreet2"}
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Addresses.objects.count(), 2)
        self.assertEqual(Addresses.objects.get(id=2).city, 'TestCity2')

    def test_get_address_list(self):
        url = reverse('addresses-list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_address(self):
        response = self.client.get('/addressess/1/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
