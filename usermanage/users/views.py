# -*- coding: utf-8 -*-
from rest_framework import viewsets

from .models import Users, Addresses
from .serializers import UsersSerializer, AddressesSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UsersSerializer


class AddressesViewSet(viewsets.ModelViewSet):
    queryset = Addresses.objects.all()
    serializer_class = AddressesSerializer
